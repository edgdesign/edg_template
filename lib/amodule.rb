require_relative './app_logger'

module AModule
	def self.foo
		AppLogger.info 'Hello from AModule'
		'bar bar bar'
	end
end