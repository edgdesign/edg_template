
before :'deploy:cleanup', :'deploy:symlink_nginx'

namespace :deploy do 
	desc 'symlink nginx config into sites enabled' 
	task :symlink_nginx do 
		on roles(:all) do 
			nginx_path = fetch(:nginx_path)
			application = fetch(:application)
			target = File.join nginx_path, application
			execute :sudo, :ln, "-sf",  "#{current_path}/config/nginx.prod.conf #{target}"
		end
	end
end