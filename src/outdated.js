//////// outdated browser warning 
// https://www.npmjs.com/package/outdated-browser-rework
// per vendor instructions - this should be bundled by itself (unsupported JS in the main bundle can otherwise prevent this from running on old browsers).
var OutdatedBrowserRework = require('outdated-browser-rework');

// TODO - should have a browserslist file that we can use for autoprefixer as well as this...
var browserOptions = {
	browserSupport: {
		'Chrome': 45, // 45 Includes Chrome for mobile devices
		'Edge': 14,
		'Safari': 9,
		'Mobile Safari': 8,
		'Firefox': 30,
		'Opera': 50,
		'Vivaldi': 1,
		'IE': 12
	},
	isUnknownBrowserOK: true
};

OutdatedBrowserRework(browserOptions);