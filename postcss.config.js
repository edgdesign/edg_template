const colorConverter = require('css-color-converter');
const autoprefixer = require('autoprefixer');
const uiVars = require('./ui-vars');

module.exports = {
	plugins: [
		require('postcss-import'), // should be first transformation applied
    require('postcss-mixins')({
      mixinsDir: __dirname + '/src/styles/mixins',
    }),
		require('postcss-simple-vars')({
			variables:function(){
				return require('./ui-vars');
			}
		}),
		require('postcss-functions')({
			functions: {
				// sets font sizes, can be used elsewhere for consistent element spacing and the like.
				fontSize: function(degree, strand) {
          if (strand == 2) {
            var base = uiVars.base2;
            var ratio = uiVars.ratio2;
          } else {
            var base = uiVars.base1;
            var ratio = uiVars.ratio1;
          }
          return `${base * Math.pow(ratio, degree)}rem`;
        },
        // returns a breakpoint -- must request a breakpoint defined in ui-vars.js -- pass "false" as the second argument if you want "max-width" instead of "min-width" (shouldn't unless there's a very compelling reason)
        getBreakpoint: function(bp, min) {
          var min = min == 'false' ? false : true; // dirty. converting to boolean as postcss-functions seems to treat all arguments as a string. if undefined, treat as true, as we want to enforce mobile-first as much as possible
          if (min) {
            return `(min-width: ${uiVars[bp]}px)`;
          } else {
            return `(max-width: ${uiVars[bp]}px)`;
          }
        },
        // returns a percentage width -- TODO - this is pretty simplistic, how can I expand to make it more useful?
        getWidth(dividend, divisor) {
          return `${(dividend / divisor) * 100}%`;
        },
				darken: function(value, frac){
					var darken = 1 - parseFloat(frac);
					var rgba = colorConverter(value).toRgbaArray();
					var r = rgba[0] * darken;
					var g = rgba[1] * darken;
					var b = rgba[2] * darken;
					return colorConverter([r,g,b]).toHexString();
				},
				lighten: function(value, frac){
					var lighten = 1 - parseFloat(frac);
					var rgba = colorConverter(value).toRgbaArray();
					var r = rgba[0] / lighten;
					var g = rgba[1] / lighten;
					var b = rgba[2] / lighten;
					return colorConverter([r,g,b]).toHexString();
				},
        setAlpha: function(value, alpha) {
          var transparency = 1 - parseFloat(alpha);
          var rgba = colorConverter(value).toRgbaArray();
          rgba[3] = transparency;
          return colorConverter(rgba).toHexString();
        }
			}
		}),
		require('postcss-nested'),
		autoprefixer({
      flexbox: 'no-2009'
    })
	]
}