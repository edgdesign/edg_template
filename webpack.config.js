const path = require('path'),
			webpack = require('webpack'),
			{ CleanWebpackPlugin } = require('clean-webpack-plugin'),
			HtmlWebpackPlugin = require('html-webpack-plugin'),
			RobotstxtPlugin = require('robotstxt-webpack-plugin'),
			MiniCssExtractPlugin = require('mini-css-extract-plugin'),
			ImageminPlugin = require('imagemin-webpack-plugin').default,
			TerserPlugin = require('terser-webpack-plugin'),
			OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');


const devMode = process.env.NODE_ENV === 'development',
			SITE_URL = 'http://edgdesign.info/';

module.exports = {
	entry: {
		main: './src/index.js',
		outdated: './src/outdated.js'
	},
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: '[name].bundle.js',
		publicPath: '/'
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'src/views/index.pug'
		}),
		new HtmlWebpackPlugin({
			filename: '404.html',
			template: 'src/views/404.pug',
			chunks: []
		}),
		new RobotstxtPlugin({
			sitemap: SITE_URL + 'sitemap.xml'
		}),
		new MiniCssExtractPlugin({
			filename: devMode ? '[name].css' : '[name].[hash].css',
			chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
		}),
		new ImageminPlugin({
			disable: devMode,
			test: /\.(jpe?g|png|gif|svg)$/i,
			jpegtran: { progressive: true }
		})
	],
	optimization: {
		minimizer: [new TerserPlugin({}), new OptimizeCSSAssetsPlugin({})]
	},
	module: {
		rules: [
			
			// pug templating
			{
				test: /\.pug$/,
				use: ['pug-loader']
			},

			// project css (exclude node_modules dependencies)
			{
				test: /\.css$/,
				exclude: /node_modules/,
				use: [
					{
						loader: devMode ? 'style-loader' : MiniCssExtractPlugin.loader
					},
					{
						loader:'css-loader',
						options: {
							modules: false
							// localIdentName: '[path]___[name]__[local]___[hash:base64:5]',
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							config: {
								path: __dirname + '/postcss.config.js'
							}
						}
					}
				]
			},

			// external css (anything in node_modules)
			{
				test: /\.css$/,
				exclude: /src/,
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							modules: false
						}
					}
				]
			},
			{
				test: /\.(gif|png|jpe?g|svg)$/i,
				loader: [
					'file-loader'
				]
			}
		]
	},
	resolve: {
		extensions: ['*', '.js']
	},
	devServer: {
		contentBase: './public',
		hot: true,
		allowedHosts: [
			'.local',
			'.ngrok.io'
		],
		host: '0.0.0.0',
		proxy: {
			'/api': {
				target: 'http://localhost:9292'
			}
		}
	}
}
