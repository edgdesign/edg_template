# Template repo

This repo is meant as a starter for simple static sites with minimal api integration or template rendering

Meant to use with sinatra, webpack, nginx.

## Setup

install ruby deps
`bundle install`

install node deps
`npm install`

## Commands

Run webpack dev server for development
`start` => `webpack-dev-server --open`
Run sinatra server
`start:server` => `bundle exec rackup`
Run webpack watch
`build:watch` => `webpack --watch`
Run webpack build
`build` => `webpack`

## Deployment

initiate a deployment
`bundle exec cap {stage} deploy`

## Sinatra Code Reloading

if you want code to reload that isnt in app.rb it must be configured.
defaults reloads everything in "./lib/**/*.rb"

## Logging with custom logger
We have setup a customer logging class in lib that allows access to the logger instance from outside the sinatra app class itself.