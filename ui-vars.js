const breakpointVars = {
	palmEnd: 650,
	get tabStart(){
		return this.palmEnd + 1;
	},
	tabEnd: 1100,
	get deskStart(){
		return this.tabEnd + 1;
	}
}

// https://color.hailpixel.com/#670001,9A0000,141414,262626,A6A6A6,E0E0E0,F7F7F7
const colorVars = {
	colorTypeDark: 'rgb(20,20,20)',
	colorTypeLight: 'rgb(247,247,247)',
	colorBrandRed: '#9a0000',
	colorBrandMaroon: '#670001',
	get colorPrimary(){
		return this.colorBrandRed;
	},
	get colorSecondary(){
		return this.colorBrandMaroon;
	},
	colorGrey1: 'rgb(224,224,224)',
  colorGrey2: 'rgb(166,166,166)',
  colorGrey3: 'rgb(38,38,38)'
}

const typographyVars = {
	baseFont: `"Lato", "Helvetica Neue", sans-serif`,
  headerFont: `"Lora", "Times", serif`,
  base1: 1.7,
  ratio1: 1.25,
  base2: 2,
  ratio2: 1.5,
}

const uiVars = {
	uiBorderRadius: '3px'
}


module.exports = Object.assign(
	{},
	breakpointVars,
	colorVars,
	typographyVars,
	uiVars
)